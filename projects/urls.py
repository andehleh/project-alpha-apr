from django.urls import path
from projects.views import (
    project_list_list,
    show_detail_detail,
    create_project_project,
)

urlpatterns = [
    path("", project_list_list, name="list_projects"),
    path("<int:id>/", show_detail_detail, name="show_project"),
    path("create/", create_project_project, name="create_project"),
]
