from django.urls import path
from tasks.views import create_task_task, task_list_list

urlpatterns = [
    path("create/", create_task_task, name="create_task"),
    path("mine/", task_list_list, name="show_my_tasks"),
]
